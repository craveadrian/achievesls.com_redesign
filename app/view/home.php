<div id="content0">
	<div class="row">
		<div class="content-left">
            <h4>WELCOME TO</h4>
            <h2>Achieve Speech and</h2>
            <h3>Language Services</h3>
            <p>
                ACHIEVE Speech and Language Services, PLLC is a growing speech pathology practice serving the communication needs of children and families in and around Houston, TX. Our office is located in the Atascocita area.
            </p>
            <p>
                Achieve Speech & Language has highly skilled staff, modern therapy equipment, assessment tools, and computer resources. Our therapists frequently attend continuing education courses to stay current in this ever-evolving field.
            </p>
            <p>
                <a href="<?php echo URL ?>services#content" class="btn">LEARN MORE</a>
            </p>
        </div>
        <div class="content-right">
            <img src="public/images/common/welcome.png" alt="welcome image">
        </div>
	</div>
</div>

<div id="content2">
    <div class="row">
        <h2>Evaulation &amp; Therapy</h2>
        <div class="content2-bot">
            <dl>
                <dt>
                    <a href="services#service1"><img src="public/images/content/e1.png" alt="Evaluations & Therapy"></a>
                </dt>
                <a href="services#service1">
                <dd>
                    FREE INFORMATIONAL
                    <span>TOUR</span>
                </dd>
                </a>
            </dl>
            <dl>
                <dt>
                    <a href="services#service2"><img src="public/images/content/e2.png" alt="Evaluations & Therapy"></a>
                </dt>
                <a href="services#service2">
                <dd>
                    FULL SPEECH &amp; LANGUAGE
                    <span>EVALUATIONS</span>
                </dd>
                </a>
            </dl>
            <dl>
                <dt>
                    <a href="services#service3"><img src="public/images/content/e3.png" alt="Evaluations & Therapy"></a>
                </dt>
                <a href="services#service3">
                <dd>
                    INDIVIDUAL THERAPY
                    <span>SESSION</span>
                </dd>
                </a>
            </dl>
            <dl>
                <dt>
                    <a href="services#service4"><img src="public/images/content/e4.png" alt="Evaluations & Therapy"></a>
                </dt>
                <a href="services#service4">
                <dd>
                    INSURANCE AND
                    <span>REFFERALS</span>
                </dd>
                </a>
            </dl>
        </div>
    </div>
</div>


<div id="content3">
    <div class="row">
        <h2>Meet The Team</h2>
        <p>
            ACHIEVE Speech and Language is comprised of highly trained and certified Speech-Language Pathologists and dedicated support staff. We
            <span>are committed to providing the absolute best in speech therapy and integrating the most up-to-date treatment techniques with a </span>
            <span>family-centered approach.</span>
        </p>
        <div class="content3-bot">
            <dl>
                <dt>
                    <img src="public/images/content/t1.png" alt="Team Image 1">
                </dt>
                <dd>
                    <h4>KANDY JENKS</h4>
                    <h5>MS, CCC-SLP</h5>
                    <p>
                        <a href="mailto:kjenks@achievesls.com">
                            kjenks@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl>
            <dl>
                <dt>
                    <img src="public/images/content/t2.png" alt="Team Image 2">
                </dt>
                <dd>
                    <h4>
                        KIM SITGREAVES
                    </h4>
                    <h5>
                        MS, CCC-SLP
                    </h5>
                    <p>
                        <a href="mailto:ksitgreaves@achievesls.com">
                            ksitgreaves@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl><dl>
                <dt>
                    <img src="public/images/content/t3.png" alt="Team Image 3">
                </dt>
                <dd>
                    <h4>
                        KATHRYN DALTRY
                    </h4>
                    <h5>
                        MS, CCC-SLP
                    </h5>
                    <p>
                        <a href="mailto:kdaltry@achievesls.com">
                            kdaltry@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl><dl>
                <dt>
                    <img src="public/images/content/t4.png" alt="Team Image 4">
                </dt>
                <dd>
                    <h4>
                        BRE HOMANN
                    </h4>
                    <h5>
                        BS, SLP-Assistant
                    </h5>
                    <p>
                        <a href="mailto:bhomann@achievesls.com">
                            bhomann@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl><dl>
                <dt>
                    <img src="public/images/content/t5.png" alt="Team Image 5">
                </dt>
                <dd>
                    <h4>
                        BETHANY RITER
                    </h4>
                    <h5>
                        MS, CCC-SLP
                    </h5>
                    <p>
                        <a href="mailto:briter@achievels.com">
                            briter@achievels.com
                        </a>
                    </p>
                </dd>
            </dl><dl>
                <dt>
                    <img src="public/images/content/t6.png" alt="Team Image 6">
                </dt>
                <dd>
                    <h4>
                        ANNA CARSRUD
                    </h4>
                    <h5>
                        MS, CCC-SLP
                    </h5>
                    <p>
                        <a href="mailto:acarsrud@achievesls.com">
                            acarsrud@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl><dl>
                <dt>
                    <img src="public/images/content/t7.png" alt="Team Image 7">
                </dt>
                <dd>
                    <h4>
                        LINDSEY JONES
                    </h4>
                    <h5>
                        MS, CCC-SLP
                    </h5>
                    <p>
                        <a href="mailto:ljones@achievesls.com">
                            ljones@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl><dl>
                <dt>
                    <img src="public/images/content/t8.png" alt="Team Image 8">
                </dt>
                <dd>
                    <h4>
                        CHANNING ARNOLD
                    </h4>
                    <h5>
                        BS, SLP-Assistant
                    </h5>
                    <p>
                        <a href="mailto:carnold@achievesls.com">
                            carnold@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl><dl>
                <dt>
                    <img src="public/images/content/t9.png" alt="Team Image 9">
                </dt>
                <dd>
                    <h4>
                        MEAGAN ORSAK
                    </h4>
                    <h5>
                        BS, SLP-Assistant
                    </h5>
                    <p>
                        <a href="mailto:morsak@achievesls.com">
                            morsak@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl><dl>
                <dt>
                    <img src="public/images/content/t10.png" alt="Team Image 10">
                </dt>
                <dd>
                    <h4>
                        MADELINE LAWSON
                    </h4>
                    <h5>
                        BS, SLP-Assistant
                    </h5>
                    <p>
                        <a href="mailto:mlawson@achievesls.com">
                            mlawson@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl><dl>
                <dt>
                    <img src="public/images/content/t11.png" alt="Team Image 11">
                </dt>
                <dd>
                    <h4>
                        KRYSTA ESTREMS
                    </h4>
                    <h5>
                        MS, CCC-SLP
                    </h5>
                    <p>
                        <a href="mailto:kestrems@achievesls.com">
                            kestrems@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl><dl>
                <dt>
                    <img src="public/images/content/t12.png" alt="Team Image 12">
                </dt>
                <dd>
                    <h4>
                        RENNIE ROGERS
                    </h4>
                    <h5>
                        BS, SLP-Assistant
                    </h5>
                    <p>
                        <a href="mailto:rrogers@achievesls.com">
                            rrogers@achievesls.com
                        </a>
                    </p>
                </dd>
            </dl>
        </div>
        <h2 class="content3-h2-bot">Now Offering Bilingual Services</h2>
    </div>
</div>


<div id="content4">
    <div class="row">
        <h2>
            Testimonials
        </h2>
        <p>
            My daughter has been going to Achieve for approximately 8 months. The
            <span>progress we’ve had has been amazing. We feel so blessed to have found </span>
            <span>Achieve, they are incredibly encouraging and helpful, my daughter </span>
            <span>absolutely adores them and has learned more than I could’ve ever </span>
            <span>imagined. I highly recommend Achieve Speech!</span>
        </p>
        <p>
            Not only is our SLP the best, the young ladies that work there are some of
            <span>the nicest people. Our whole experience has been nothing but positive. </span>
            <span>Thank you so much for helping my baby girl.</span>
        </p>
        <p>
            - ARLENE CASTILLO
            <span class="stars">&#9733;&#9733;&#9733;&#9733;&#9733;</span>
        </p>
        <p>
            <a href="<?php echo URL?>reviews#content" class="btn">
                READ MORE
            </a>
        </p>
    </div>
</div>

<div id="content5">
    <div class="row">
        <img src="public/images/common/logo.png" alt="logo" id="footer-logo">
        <p>
            ACHIEVE Speech and Language offers the full range of services for children and adults requiring communication
            <span>intervention. We offer the LEAP program (preschool group), Brunch bunch (feeding group),</span> <span>Social language group (school age), and Reading groups (school age)</span>
        </p>
        <div class="content5-top">
            <img src="public/images/content/a1.png" alt="">
            <img src="public/images/content/a2.png" alt="">
            <img src="public/images/content/a3.png" alt="">
            <img src="public/images/content/a4.png" alt="">
        </div>
        <div class="content5-bot">
            <dl>
                <dt>
                    <img src="public/images/common/email.png" alt="">
                </dt>
                <dd>
                    <p>EMAIL</p>
                    <h3><?php $this->info(['email', 'mailto']) ?></h3>
                </dd>
            </dl>
            <dl>
                <dt>
                    <img src="public/images/common/phone.png" alt="">
                </dt>
                <dd>
                    <p>PHONE</p>
                    <h4><?php $this->info(["phone","tel"]) ?></h4>
                </dd>
            </dl>
            <dl>
                <dt>
                    <img src="public/images/common/location.png" alt="">
                </dt>
                <dd>
                    <p>LOCATION</p>
                    <h5><a href="https://goo.gl/maps/bYBHoEKB9bU2" target="_blank"><?php $this->info('address') ?></a></h5>
                </dd>
            </dl>
        </div>
    </div>
</div>
