<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
		<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
		<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
		<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">

				<div class="header-top">
					<div class="header-top-left">
						<img src="public/images/common/logo.png" alt="logo" id="header-logo">
					</div>
					<div class="header-top-right">
						<div class="contT">
								<p>CONTACT US TODAY: <span> <?php $this->info(['phone','tel']) ?></span></p>
						</div>
						<div class="emailT">
								<p>EMAIL US: <span> <?php $this->info(['email','mailto']) ?></span></p>
						</div>
						<div>
							<img src="public/images/common/fb.png" alt="facebook logo">
						</div>
					</div>
				</div> <!-- /header-top -->

			</div>
		</div>
		<div id="header-bot">
			<div class="row">
				<nav>
					<a href="#" id="pull">
						<img src="public/images/nav-icon.png" alt="">
					</a>
					<ul>
						<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
						<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
						<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
						<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews#content">REVIEWS</a></li>
						<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
						<li <?php $this->helpers->isActiveMenu("forms"); ?>><a href="<?php echo URL ?>forms#content">FORMS</a></li>
					</ul>
				</nav>
			</div>
		</div> <!-- /header-bot -->
	</header>

	<?php if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<img src="public/images/common/banner-image.png" alt="banner image" id="banner-image">
			</div>
		</div>
	<?php endif; ?>
