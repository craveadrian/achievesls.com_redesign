<div id="content">
    <div class="row" id="services-page">
        <h1 id="top">Services</h1>
        <p>
            ACHIEVE Speech and Language offers the full range of services for children and adults requiring communication intervention.
            We are also proud to feature the LEAP Program each summer.
        </p>

        <div id="services-wrapper">
            <div id="services-navigation">

                <ul>
                    <li><h3>Evaluation & Therapy</h3></li>
                    <li>
                        <a id="link1" href="#service0"> Summer Programs</a>
                    </li>
                    <li>
                        <a id="link1" href="#service1"> Free Informational Tour</a>
                    </li>
                    <li>
                        <a id="link2" href="#service2"> Full Speech & Language Evaluations</a>
                    </li>
                    <li>
                        <a id="link3" href="#service3"> Individual Therapy Sessions</a>
                    </li>
                    <li>
                        <a id="link4" href="#service4"> Insurance & Referrals</a>
                    </li>
                    <li><h3>Achieve Speech & Language Specializes in:</h3></li>
                    <li>
                        <a id="link5" href="#service5"> Autism</a>
                    </li>
                    <li>
                        <a id="link6" href="#service6"> Adult Therapy</a>
                    </li>
                    <li>
                        <a id="link7" href="#service7"> Accent Reduction</a>
                    </li>
                    <li>
                        <a id="link8" href="#service8"> Aphasia</a>
                    </li>
                    <li>
                        <a id="link9" href="#service9"> Articulation</a>
                    </li>
                    <li>
                        <a id="link10" href="#service10"> Auditory Processing Disorder</a>
                    </li>
                    <li>
                        <a id="link11" href="#service11"> Childhood Apraxia of Speech (CAS)</a>
                    </li>
                    <li>
                        <a id="link12" href="#service12"> Developmental Delays</a>
                    </li>
                    <li>
                        <a id="link13" href="#service13"> Ear Infections</a>
                    </li>
                    <li>
                        <a id="link14" href="#service14"> Feeding Therapy</a>
                    </li>
                    <li>
                        <a id="link15" href="#service15"> Genetic Disorders</a>
                    </li>
                    <li>
                        <a id="link16" href="#service16"> Language Enrichment Around Play (LEAP Program)</a>
                    </li>
                    <li>
                        <a id="link17" href="#service17"> Language Disorders</a>
                    </li>
                    <li>
                        <a id="link18" href="#service18"> Literacy Instruction</a>
                    </li>
                    <li>
                        <a id="link19" href="#service19"> Oral Function Therapy</a>
                    </li>
                    <li>
                        <a id="link20" href="#service20"> Parent Training</a>
                    </li>
                    <li>
                        <a id="link21" href="#service21"> Phonological Process Disorder</a>
                    </li>
                    <li>
                        <a id="link22" href="#service22"> Play-Based Therapy</a>
                    </li>
                    <li>
                        <a id="link23" href="#service23"> Research-Based Therapy</a>
                    </li>
                    <li>
                        <a id="link24" href="#service24"> Social Language Programs</a>
                    </li>
                    <li>
                        <a id="link25" href="#service25"> Stuttering</a>
                    </li>
                    <li>
                        <a id="link26" href="#service26"> Tongue-Tie</a>
                    </li>
                    <li>
                        <a id="link27" href="#service27"> Trainings and Seminars</a>
                    </li>
                    <li>
                        <a id="link28" href="#service28"> Voice</a>
                    </li>
                </ul>
            </div>

            <div id="services-content">
              <div id="service0">
                  <?php include 'services/service0.php'; ?>
              </div>
                <div id="service1">
                    <?php include 'services/service1.php'; ?>
                </div>
                <div id="service2">
                    <?php include 'services/service2.php'; ?>
                </div>
                <div id="service3">
                    <?php include 'services/service3.php'; ?>
                </div>
                <div id="service4">
                    <?php include 'services/service4.php'; ?>
                </div>
                <div id="service5">
                    <?php include 'services/service5.php'; ?>
                </div>
                <div id="service6">
                    <?php include 'services/service6.php'; ?>
                </div>
                <div id="service7">
                    <?php include 'services/service7.php'; ?>
                </div>
                <div id="service8">
                    <?php include 'services/service8.php'; ?>
                </div>
                <div id="service9">
                    <?php include 'services/service9.php'; ?>
                </div>
                <div id="service10">
                    <?php include 'services/service10.php'; ?>
                </div>
                <div id="service11">
                    <?php include 'services/service11.php'; ?>
                </div>
                <div id="service12">
                    <?php include 'services/service12.php'; ?>
                </div>
                <div id="service13">
                    <?php include 'services/service13.php'; ?>
                </div>
                <div id="service14">
                    <?php include 'services/service14.php'; ?>
                </div>
                <div id="service15">
                    <?php include 'services/service15.php'; ?>
                </div>
                <div id="service16">
                    <?php include 'services/service16.php'; ?>
                </div>
                <div id="service17">
                    <?php include 'services/service17.php'; ?>
                </div>
                <div id="service18">
                    <?php include 'services/service18.php'; ?>
                </div>
                <div id="service19">
                    <?php include 'services/service19.php'; ?>
                </div>
                <div id="service20">
                    <?php include 'services/service20.php'; ?>
                </div>
                <div id="service21">
                    <?php include 'services/service21.php'; ?>
                </div>
                <div id="service22">
                    <?php include 'services/service22.php'; ?>
                </div>
                <div id="service23">
                    <?php include 'services/service23.php'; ?>
                </div>
                <div id="service24">
                    <?php include 'services/service24.php'; ?>
                </div>
                <div id="service25">
                    <?php include 'services/service25.php'; ?>
                </div>
                <div id="service26">
                    <?php include 'services/service26.php'; ?>
                </div>
                <div id="service27">
                    <?php include 'services/service27.php'; ?>
                </div>
                <div id="service28">
                    <?php include 'services/service28.php'; ?>
                </div>
            </div>
        </div>

    <a href="#top" class="btt">&#9978;</a>

    </div>
</div>
