<h2>Parent Training</h2>
<p>
    Parents and family members are welcomed and encouraged to attend therapy sessions to gain skills necessary to work on therapy
    goals outside of the session. We also have an observation room with a two-way mirror so the patient can be observed without
    actually being present in the therapy room.
</p>