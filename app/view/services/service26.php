<h2>Tongue Tie</h2>
<p>
    Tongue-tie or ankyloglossia is caused when the membrane under the tongue (the frenulum) extends excessively toward the tip
    of tongue. This extended attachement of the frenulum limits the mobility of the tongue, often giving it a characteristic
    heart shape when extended. When it comes to whether or not clipping the frenulum is necessary you should consult your
    doctor as well as a speech language pathologist. There are clearly some babies who have significant difficulty breastfeeding
    because of tongue-tie. These infants may actually have trouble gaining weight and cause the mother a good deal of breast
    tenderness from a disorganized suck due to tongue-tie. If your child is older, having a restricted frenulum may affect
    development of certain speech sounds and cause articulation disorders as their speech develops. Many times, as the child
    gets older, the frenulum stretches such that speech problems do not occur. You may find conflicting answers from different
    doctors and books because there simply is no consensus on this issue. That being the case, many physicians deal with
    this issue on a case-by-case basis and recommend clipping only when problems are being caused by the tongue-tie rather
    than clipping it on the assumption that the tongue-tie might cause problems in the future.
</p>