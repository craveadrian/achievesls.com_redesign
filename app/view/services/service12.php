<h2>Developmental Delays</h2>
<p>
    Every child develops at a different rate, but most go through the same stages. Listed below are the average ages of some
    important language and comprehension milestones as developed by the
    <strong>American Speech-Language-Hearing Association</strong> (ASHA). Please note that with any developmental timeline, these
    stages may be varied and perhaps met in a slightly different order. A child who accomplishes these milestones differently
    may not necessarily have a developmental delay or speech disorder and a child who hits these stages early is not necessarily
    a prodigy.
</p>
<ul>
    <li>
        <strong>Birth to 3 months</strong>
        <ul>
            <li>Startles to loud sounds</li>
            <li>Smiles when spoken to</li>
            <li>Responds to pleasure with 'cooing' noises</li>
        </ul>
    </li>
    <li>
        <strong>4 months to 6 months</strong>
        <ul>
            <li>Notices and pays attention to sounds and music</li>
            <li>Shifts eyes in direction of sounds</li>
            <li>Makes babbling noises that resemble speech</li>
        </ul>
    </li>

    <li>
        <strong>7 months to 1 year</strong>
        <ul>
            <li>Recognizes basic familiar words such as cup or ball</li>
            <li>Imitates different speech sounds</li>
            <li>Produces first words such as bye-bye or mama</li>
        </ul>
    </li>

    <li>
        <strong>1 year to 2 years</strong>
        <ul>
            <li>Listens to simple stories</li>
            <li>Identifies pictures by name when directed (point to the cow, e.g.)</li>
            <li>Speaks two-word sentences such as More juice? or Where daddy?</li>
        </ul>
    </li>

    <li>
        <strong>2 years to 3 years</strong>
        <ul>
            <li>Understands differences in meaning for basic words (up-down or in-out)</li>
            <li>Produces three-word sentences</li>
            <li>Can name most objects</li>
        </ul>
    </li>

    <li>
        <strong>3 years to 4 years</strong>
        <ul>
            <li>Understands questions</li>
            <li>Talks about events</li>
            <li>Speech is understood by most people</li>
        </ul>
    </li>

    <li>
        <strong>4 years to 5 years</strong>
        <ul>
            <li>pays attention and responds to stories and questions</li>
            <li>speaks clearly</li>
            <li>tells detailed, ordered stories</li>
        </ul>
    </li>
</ul>

<p>
    Problems can arise at any stage of development, as well as much later in life. They can be the result of a congenital defect,
    a developmental disorder, or an injury. If a problem is suspected, an assessment should be made by a speech language
    pathologist who can confirm or deny your concerns as well as diagnose and treat communication disorders. Early intervention
    is essential when working with children. Early detection and early intervention can have an impact on how the child responds
    to therapy and how long the child requires therapy. It may also be a good idea to take your child to his/her pediatrician
    if you are concerned about other areas of development besides speech. (i.e. behavior, gross/fine motor skills)
</p>
