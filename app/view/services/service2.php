<h2>Full Speech & Language Evaluations</h2>
<p>
    Evaluations are scheduled for one hour sessions in which the patient will meet with a therapist for standardized and non-standardized
    testing. Once the evaluation portion of the session is completed the therapist will counsel the family on results and
    therapy recommendations. Following an initial evaluation, the therapist will prepare a formal written evaluation which
    will be submitted to the patient's primary care physician. This evaluation will serve as a prescription for services
    for six months. Re-evaluations are completed every six months to measure progress and update goals.
</p>