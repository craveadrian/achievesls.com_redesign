<h2>Insurance & Referrals</h2>
<p>
    In Texas, it is not necessary to obtain a doctor's referral to see a speech pathologist in a private practice or public school
    setting. However, some private health insurances may require a doctor's referral letter before they will pay benefits
    for speech pathology services. As a courtesy, Achieve gladly obtains referrals and prescriptions for speech therapy.
    It is always a good idea to call the member services department to have them quote what steps are needed to receive speech
    therapy. It may also be beneficial to examine insurance benefits booklets to be aware coverage limitations. Insurance
    companies provide many different policies that cover different services. Not all insurance plans at the same insurance
    company provide for the same coverage.
</p>
<p>
    Though families generally do not think of speech therapy when purchasing health insurance, communication is a necessary life
    function and, when disrupted, is life altering. Many health insurance plans cover speech therapy. There are often limitations
    to the amount of coverage a member will receive, but in many cases the coverage is enough to provide for the needs of
    the patient. Prior to a first appointment for a screening or initial evaluation, Achieve will call to verify benefits
    under insurance policies for speech therapy services. Achieve notifies patients of any limitations or exclusions for
    speech therapy. Typically an insurance company will not deny the referral to the initial evaluation. Coverage for services
    will be based on information obtained during the initial evaluation.
</p>