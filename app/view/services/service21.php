<h2>Phonological Process Disorder</h2>
<p>
    A phonological process disorder involves patterns of sound errors. For example, substituting all sounds made in the back
    of the mouth like "k" and "g" for those in the front of the mouth like "t" and "d" (e.g., saying "tup" for "cup" or "das"
    for "gas").
</p>
<p>
    Another rule of speech is that some words start with two consonants (consonant clusters), such as "broken" or "spoon". When
    children don't follow this rule and say only one of the sounds ("boken" for "broken" or "poon" for "spoon"), it is more
    difficult for the listener to understand the child. While it is common for young children learning speech to leave one
    of the sounds out of the word, it is not expected as a child gets older. If a child continues to demonstrate such cluster
    reduction, he or she may have a phonological process disorder. The following is a list of phonological processes and
    when they should discontinue:
</p>
<p>
    By the age of 3, the following should discontinue:
</p>
<ul>
    <li>Voicing: pig/big, pig/pick</li>
    <li>Final consonant deletion: comb=coe</li>
    <li>Stopping /f/ & /s/: fish=tish, soap=dope</li>
</ul>
<p>
    By the age of 3 ½, the following should discontinue:
</p>
<ul>
    <li>Fronting: car=tar, ship=sip</li>
    <li>Assimilation (consonant harmony) mine=mime, kittycat=tittytat</li>
    <li>Stopping /v/ & /z/: very=berry, zoo=doo</li>
</ul>
<p>By the age of 4, the following should discontinue:</p>
<ul>
    <li>Weak syllable deletion: potato=tato, banana=nana</li>
    <li>Custer reduction: spoon=poon, clean=keen</li>
    <li>Stopping sh, j, & ch: shop=dop, jump=dump, chair=tare</li>
</ul>
<p>
    By the age of 5, the following should discontinue:
</p>
<ul>
    <li>Gliding of Liquids: run=one, leg=weg</li>
    <li>Stopping th: thing=ting, them=dem</li>
</ul>