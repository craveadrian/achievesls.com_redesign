<h2>Childhood Apraxia of Speech- (CAS)</h2>
<p>
    Therapists at Achieve specialize in working with children with Apraxia and have trained with Nancy Kaufman, who specializes
    in treating children with CAS. Children with CAS have great difficulty planning and producing the precise, highly refined
    and specific series of movements of the tongue, lips, jaw and palate that are necessary for intelligible speech. Specifically,
    these children may make inconsistent errors on consonants and vowels in repeated productions, have difficulty producing
    multi-syllable words accurately, and exhibit inappropriate prosody. CAS may occur as a result of known neurological impairment,
    in association with other complex neurobehavioral disorders, or as a separate, isolated, neurogenic speech sound disorder.
    Children with CAS have early and persistent problems in speech which often leads to difficulty with expressive language
    and the phonological foundations for literacy. Depending on the severity of CAS there may also be a need for augmentative
    and alternative communication and assistive technology. Intensive and individualized treatment of CAS focusing on repetitive
    planning, programming, and production practice has been shown to improve intelligibility and communication functioning.
    The primary goal is to improve the motoric aspects of the child's speech production, and this has been proven to be best
    accomplished through individual, one-on-one therapy.
</p>
<p>
    Another rule of thumb is to consider how clear a child's speech sounds:
</p>
<ul>
    <li>By 18 months a child's speech is normally 25% intelligible.</li>
    <li>By 24 months a child's speech is normally 50 -75% intelligible.</li>
    <li>By 36 months a child's speech is normally 75-100% intelligible.</li>
</ul>