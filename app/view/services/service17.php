<h2>Language Disorders</h2>
<p>
    Language disorders are described as difficulties with the comprehension, production, and use of language. Deficits in language
    can have a pervasive impact on a child's development, success in school, relationships, and emotional well-being. 5%
    of preschool children exhibit a significant limitation in language ability not caused by problems with hearing, intelligence,
    or neurological structure. They are hampered in conveying and receiving information. Children with impaired language
    skills have a hard time fulfilling their potential unless efforts are made to improve their language skills. Clinical
    research has documented that children with language disorders benefit from treatment provided by a speech language pathologist.
</p>
<p>
    More than 200 research studies report that language intervention is effective for the majority of those who participate.
    Language treatment addresses functional communication skills, thereby improving the quality of life for the child, by
    enhancing social, academic, and vocational situations. Children who struggle in multiple areas of language can learn
    targeted language skills in those specific areas.
</p>
<p>
    The role of the speech language pathologist is to determine the child's receptive and expressive language skills using standardized
    or informal assessment. The child's language skills are then compared to other observations or normative data that reflect
    the typical performance of a child that age or developmental level. This comparison helps to determine the presence or
    absence of significant deficiencies and exactly where those significant weaknesses exist. The speech language pathologist
    selects functional treatment goals from the weakness displayed on the assessment and based on their usefulness in home,
    educational, and community settings. The primary goal of language treatment is to increase the frequency and quality
    of language the child can use and understand to age-appropriate levels. Other aspects of treatment may be language production,
    increasing vocabulary, improving interaction skills, or using augmentative/alternative communication systems. (Modified
    from ASHA Treatment Efficacy Summary, Goldstein, H.)
</p>