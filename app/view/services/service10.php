<h2>Auditory Processing Disorder</h2>
<p>
    Individuals with APD may exhibit a variety of listening and related complaints. For example, they may have difficulty understanding
    speech in noisy environments, following directions, and discriminating similar-sounding speech sounds. Sometimes they
    may behave as if a hearing loss is present, often asking for repetition or clarification. In school, children with APD
    may have difficulty with spelling, reading, and understanding information presented verbally in the classroom. Often
    their performance in classes that don't rely heavily on listening is much better, and they typically are able to complete
    a task independently once they know what is expected of them. However, it is critical to understand that these same types
    of symptoms may be apparent in children who do not exhibit APD. Therefore, we should always keep in mind that not all
    language and learning problems are due to APD, and all cases of APD do not lead to language and learning problems. APD
    cannot be diagnosed from a symptoms checklist. No matter how many symptoms of APD a child may have, only careful and
    accurate diagnostics can determine the underlying cause. The actual diagnosis of APD must be made by an audiologist,
    however symptoms of APD may be treated by a speech language pathologist.
</p>
<p>
    Treatment of APD generally focuses on three primary areas: changing the learning or communication environment, recruiting
    higher-order skills to help compensate for the disorder, and remediation of the auditory deficit itself. Compensatory
    strategies usually consist of suggestions for assisting listeners in strengthening central resources (language, problem-solving,
    memory, attention, and other cognitive skills) so that they can be used to help overcome the auditory disorder. In addition,
    many compensatory strategy approaches teach children with APD to take responsibility for their own listening success
    or failure and to be an active participant in daily listening activities through a variety of active listening and problem-solving
    techniques.
</p>