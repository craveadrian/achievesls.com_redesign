<h2>Literacy Instruction</h2>
<p>
    Achieve therapists have been trained in implementing the Sound Sensible and SPIRE programs in order to directly target reading
    skills in preschoolers and school-aged children. If a child has a history of learning deficits in the area of reading,
    these programs are designed to pinpoint areas of growth to strengthen reading and reading comprehension skills. This
    program is particularly effective if you suspect dyslexia or symptoms of a learning disability. Therapists at Achieve
    specialize in important phonological awareness skills children may miss in a regular school setting. These include: rhyming,
    segmenting/blending sounds, isolating/deleting sounds, and decoding.
</p>