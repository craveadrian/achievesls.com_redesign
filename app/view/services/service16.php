<h2>Language Enrichment Around Play (LEAP Program)</h2>
<p>
    The LEAP program is designed to target early speech, language, and learning skills for children of all ages. LEAP classes
    are offered in small groups to maximize exposure to all concepts. Classes are 1 hour in length, 2 times a week, and are
    offered each summer. All classes are facilitated by a licensed, certified Speech-Language Pathologist.
</p>
<p>
    The LEAP classes are offered to children with and without speech, language, or learning disabilities. The class is open for
    any and all children who will benefit from:
</p>
<ul>
    <li>Preschool skills</li>
    <li>Kindergarten readiness skills</li>
    <li>Exposure to structured learning environment</li>
    <li>Maintenance and continued learning throughout summer</li>
    <li>Exposure to peer interactions in small group</li>
</ul>
<p>
    The LEAP classes target skills in the following areas:
</p>
<ul>
    <li>Vocabulary Building</li>
    <li>Phonemic Awareness</li>
    <li>Pre-Literacy Skills</li>
    <li>Social Language Skills</li>
    <li>Other basic preschool/kindergarten readiness skills</li>
</ul>