<h2>Aphasia-(Related to Stroke or Tramatic Brain Injury)</h2>
<p>
    Aphasia is a language disorder that typically occurs in individuals after some incident of brain damage. Individuals who
    previously expressed themselves and understood communication through speaking, reading, or writing suddenly find themselves
    unable or limited in their ability to participate in these previous activities. Aphasia can lead to social isolation.
    The most common cause of aphasia is stroke. It is estimated that over 1 million Americans have aphasia. Clinical evidence
    and research documents that individuals with aphasia benefit from the services of speech language pathologists.
</p>
<p>
    One recent study indicated that people who have developed aphasia and receive weekly speech therapy make significantly greater
    improvement than individuals with aphasia who are not treated. Other studies conducted support the value of treatment
    for aphasia, particularly as it applies to individuals who have become aphasic as the result of a single, left hemisphere
    stroke. Improvements have been noted in terms of both quality and quantity of language. Research also suggests that treatment
    can be effective when it is provided on a continuous basis after the stroke.
</p>
<p>
    The role of the speech language pathologist is to assess the communication impairment, develop a treatment program to help
    individuals with aphasia regain as much of their communication skills as possible, and to develop strategies to compensate
    for deficient skills. The SLP may also counsel family members and other caregivers about the individual's aphasia and
    help them assist in generalization of treatment gains. Many individuals with aphasia are able to return to work and leisure
    activities after treatment.
</p>