<h2>Free Informational Tour</h2>
<p>
    We gladly offer a free informational tour which includes a fifteen minute tour of our facility and a brief meeting with a
    licensed, certified therapist. At this time any questions regarding speech therapy and the patient's
    specific needs will be answered.
</p>