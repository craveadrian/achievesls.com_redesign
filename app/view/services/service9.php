<h2>Articulation</h2>
<p>An articulation disorder involves difficulties making speech sounds. Sounds can be substituted, left off, added or changed. These errors may make it hard for people to understand the child.</p>


<p>Young children often make speech errors. For instance, many young children sound like they are making a "w" sound for an "r" sound (e.g., "wabbit" for "rabbit") or may leave sounds out of words, such as "nana" for "banana." The child may have an articulation disorder if these errors continue past age-expected norms.</p>


<p>The following is a rule of thumb for when sounds should be developed and the order of acquisition of speech sounds:</p>


<p>By the age of 2, a child should be able to combine these simple consonant sounds into 2-word phrases, such as 'my toe'. A two year old should be at least 50-75% intelligible.</p>


<p>&#9632; m, n, p, h, w, b, t, d</p>

<p>By the age of 3, a child should be able to combine the previous simple consonants and following complex consonant sounds into 3-word phrases such as, 'I eat food'. A three year old should be 75-100% intelligible.</p>


<p>&#9632; k, g, f , y, ng</p>

<p>By the age of 4, a child should be able to combine 4-word sentences and be 100% intelligible. If your child continues to have sound errors by the age of four, a speech evaluation may be warranted.</p>


<p>&#9632; sh, ch, y, j, s, z, v</p>

<p>Between the ages of 5-6 a child should begin to acquire later developing sounds which include:</p>


<p>&#9632; l, r, th, v, r-blends, s-blends, l-blends</p>
