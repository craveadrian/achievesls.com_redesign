<h2>Voice</h2>
<p>
    Achieve therapists are experienced with treating symptoms in adults and children consistent with hoarse, harsh or breathy
    vocal quality. Problems with voice include, but are not limited to, vocal nodules, polyps, vocal cord dysfunction, paralyzed
    vocal cords, velopharyngeal dysfunction, dysphonia, aphonia, and chronic hoarseness. A referral to an ENT must be made
    prior to any treatment to rule out any structural deviations and aid in appropriate treatment planning. Other symptoms
    include:
</p>
<ul>
    <li>Volume level consistently or intermittently too loud or too soft</li>
    <li>Pitch inappropriate for an individual's age and gender</li>
    <li>Soreness or pain in neck, sensation of something in throat</li>
    <li>Complaints of vocal fatigue and a need to increase vocal effort to speak</li>
    <li>Presence of vocal tremor</li>
    <li>Inappropriate resonance</li>
</ul>

<p>
    ACHIEVE Speech and Language Services accepts insurance, Medicaid and all major credit cards. We follow all national HIPAA
    guidelines to protect your privacy. For more information about Speech-Language Pathology and communication disorders,
    please visit www.ASHA.org.
</p>