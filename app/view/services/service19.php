<h2>Oral Function Therapy</h2>
<p>
    Therapists at Achieved are certified in Sara Rosenfeld-Johnson's TalkTools® hierarchies which encourage oral placement therapy
    techniques and incorporate tools specifically designed to help patients increase awareness, placement, endurance, muscle
    memory and accurate production of speech. Our therapists utilize a series of user-friendly products to correspond with
    a hierarchical approach to oral placement therapy. Many of these items, including the original Horn and Straw Hierarchies
    and Jaw Grading Bite Blocks, are now household names in the oral placement field and are available exclusively through
    TalkTools Therapy™. Through this program, Achieve therapists provide the absolute best instruction for strengthening
    and improving respiration, phonation, resonation, articulation, muscle grading and dissociation of movement.
</p>