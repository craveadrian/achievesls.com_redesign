<h2>Autism</h2>
<p>
    The number of children being diagnosed with Autistic Spectrum Disorders (ASD) has been on the rise for many years. Currently,
    statistics show that 1 in 150 children (1 in 59 boys) will be diagnosed with ASD. These children will require intervention
    for social, emotional, or behavioral differences. Early identification and treatment for these children is pivotal to
    achieving long-term goals and success in life. Achieve can provide professional advice and therapy including parent education,
    social playgroups, parent-child training, and/or individual speech therapy. Early signs of Autism include:
</p>
<ul>
    <li>A baby who shows little to no affect when near loved ones</li>
    <li>A young child who displays little to no awareness of social surroundings</li>
    <li>An 18 month old who has lost previously mastered words and phrases</li>
    <li>A 2 year old who is not yet talking</li>
    <li>A toddler who does not show interest in peers and would rather play alone</li>
    <li>A child with repetitive, stereotypical behaviors</li>
    <li>A child with an above-age-expected knowledge of any one subject</li>
    <li>A 2 year old who does not play with toys in a purposeful way</li>
    <li>A child who is overly sensitive to sound or touch</li>
    <li>A child who typically displays an overactive or underactive pattern of behavior</li>
</ul>
<p>
    While any one of the above concerns may be seen in any child, it is the combination of multiple concerns that warrants a
    referral. By working together in the community to promote early identification and intervention, families can begin to
    feel hopeful about their child's future once again.
</p>
