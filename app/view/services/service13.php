<h2>Ear Infections</h2>
<p>
    Ear infections are the most commonly diagnosed childhood illness in the United States; second only to the common cold. More
    than 3 in 4 children have had at least one ear infection by the time they reach 3 years of age. When a child has fluid
    in the middle ear, the fluid reduces sound traveling through the middle ear. Sounds may be muffled or not heard at all.
    Children with middle ear fluid will generally have a mild or moderate temporary hearing loss. The sound your child hears
    would be comparable to you plugging your ears with your fingers.
</p>
<p>
    Children develop ear infections more frequently in the first 2 to 4 years of life for several reasons: Their eustachian tubes
    are shorter and more horizontal than those of adults, which allows bacteria and viruses to find their way into the middle
    ear more easily. Also, the adenoids, which are gland-like structures located in the back of the throat near the end of
    the eustachian tubes, are large in children and can block the opening of the tubes and prevent them from functioning
    properly. Both of these structural differences can effect hearing and as a result, impact speech and language development.
    Children who have had ear infections should be monitored closely for appropriate speech and language development.
</p>
<p>
    The following are a few things to look for in your child if you suspect your child has an ear infection. Your child may:
</p>
<ul>
    <li>Not respond to soft sounds</li>
    <li>Turn up the television or radio</li>
    <li>Talk louder</li>
    <li>Tug or pull at one or both ears</li>
    <li>Appear to be inattentive at school</li>
</ul>
<p>
    Children with persistent otitis media (lasting longer than 3 months) should be reexamined periodically (every 3 to 6 months)
    by their doctors. Certain children, such as those with persistent hearing loss or speech delay, may require ear tube
    surgery. In some cases, an ear, nose, and throat doctor will suggest surgically inserting tubes (called tympanostomy
    tubes) in the eardrum. These tubes allow fluid to drain from the middle ear and help to equalize the pressure in the
    ear when the Eustachian tube is not functioning properly. (Joel Klein, MD, June 2008, KidsHealth.org)
</p>
<p>
    A speech language pathologist should be consulted if you or your pediatrician feels that your child's speech and/or language
    development is behind or is compromised due to frequent bouts of otitis media. The following are suggestions for you
    when interacting with your child at home and can be used to help all children become better listeners.
</p>
<ul>
    <li>Get within three feet of your child before speaking</li>
    <li>Get your child's attention before speaking</li>
    <li>Face your child and speak clearly with a normal tone and normal loudness</li>
    <li>Use visual cues such as moving your hands and showing pictures in addition to using speech</li>
    <li>Seat your child near adults and children who are speaking</li>
    <li>Speak clearly and repeat important words, but use natural speaking tones and pattern</li>
    <li>Check often to make sure your child understands what is said</li>
    <li>Stand still when talking to your child to decrease distractions</li>
</ul>

<p>
    (Joanne E. Roberts, Ph.D. & Susan A. Zeisel, Ed. D., Ear Infections and Language Development, Collaborative brochure with
    American Speech-Language-Hearing Association and the National Center for Early Development & Learning)
</p>
