<h2>Feeding Therapy</h2>
<p>
    Therapists at Achieve are certified in the SOS (Sequential-Oral-Sensory) Approach to Feeding Program. This approach can be
    beneficial and make mealtimes more pleasant. Children may be considered a 'picky eater' or 'problem feeder' if one or
    more of the following issues are present:
</p>
<ul>
    <li>Difficulty controlling food in his mouth</li>
    <li>Gags or is unable to tolerate certain foods or textures</li>
    <li>Decreased range or variety of foods</li>
    <li>Eats less than 30 different foods</li>
    <li>Unable to tolerate new foods on plate</li>
    <li>Eats different foods than the rest of the family.</li>
</ul>
<p>
    The SOS Approach to Feeding is a Transdisciplinary Program for assessing and treating children with feeding and weight/growth
    difficulties. It has been developed over the course of 20 years through the clinical work of Dr. Kay Toomey, in conjunction
    with colleagues from several different disciplines including: Pediatricians, Occupational Therapists, Registered Dietitians,
    and Speech Pathologists/Therapists. This program integrates motor, oral, behavioral/learning, medical, sensory and nutritional
    factors and approaches in order to comprehensively evaluate and manage children with feeding/growth problems. It is based
    on, and grounded philosophically in, the "normal" developmental steps, stages and skills of feeding found in typically
    developing children. The treatment component of the program utilizes these typical developmental steps towards feeding
    to create a systematic desensitization hierarchy of skills/behaviors necessary for children to progress with eating various
    textures, and with growing at an appropriate rate for them. The assessment component of the program makes sure that all
    physical reasons for atypical feeding development are examined and appropriately treated medically. In addition, the
    SOS Approach works to identify any nutritional deficits and to develop recommendations as appropriate to each individual
    child's growth parameters and needs. Skills across all developmental areas are also assessed with regards to feeding,
    as well as an examination of learning capabilities with regards to using the SOS program.
</p>
