    <div id="content">
        <div class="row">
            <h1>Reviews</h1>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 06/06/2018</h3>
              <p class="review-content">I don't think my little boy would be getting awards for reading without this place! He looks forward to going and knows they care for him so much.
                <blockquote>
                  <span>Achieve Speech & Language Services : </span> Thanks, Michelle! We love your entire family! One of our Achieve favorites.
                </blockquote>
              </p>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Michelle Griffith</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 02/23/2018</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Stephanie Zurovec Morris</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 05/12/2017</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Missy Jo Pouncey</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 05/01/2017</h3>
              <p class="review-content"> My daughter has been going to Achieve for approximately 8 months under the care of Alex. The progress we've had has been amazing. We feel so blessed to have found Alex, not only is she incredibly encouraging and helpful but my daughter absolutely adores her and has learned more than I could've ever imagined. I highly recommend Achieve Speech! Not only is our SLP the best, the young ladies that work there are some of the nicest people. Our whole experience has been nothing but positive. Thank you so much for helping my baby girl.
                <blockquote>
                  <span>Alexandra Douglass: </span>Thank you so much for you sweet words! I have loved getting to work with her!
                </blockquote>
              </p>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Arlene Castillo</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 05/12/2017</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Mercedes Ivy</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 05/01/2017</h3>
              <p class="review-content">  We have been going to Achieve for over 2 years. I have nothing but love for these women. They work hard and get results! Every single employee is a pleasure to work with and they all go above and beyond to do what's needed. You know they're awesome when your 2 year old, who never leaves your side, goes running to the back with their therapist every. single. time. Much love Achieve...keep up the amazing work!
                <blockquote>
                  <span>Achieve Speech & Language Services : </span>Thank you for your kind words, Amanda! We love working with your family!
                </blockquote>
              </p>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Amanda Schmidt</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 02/21/2017</h3>
              <p class="review-content"> My daughter has been seeing Kathryn for three years and we absolutely love her and everyone at Achieve!
              </p>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Robin Gray Christeson</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 02/21/2017</h3>
              <p class="review-content"> My daughter has been seeing Kathryn for three years and we absolutely love her and everyone at Achieve!
                <blockquote>
                  <span>Kathryn Daltry: </span>Thank you for your kind words-I have loved working with your sweet girl! She is such a joy to have on my schedule!
                </blockquote>
              </p>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Robin Gray Christeson</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 02/13/2017</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Kirk Higbie</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 02/09/2017</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Angela Hill Arteaga</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 02/08/2017</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Karri Moore</a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 02/08/2017</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">McKenzie Moore </a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 02/08/2017</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Loren Moore </a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 02/08/2017</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Amanda Marshall Comstock </a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 11/16/2016</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Nichole Campa </a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 09/22/2016</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Eliseo Arteaga III  </a>
              </h4>
            </div>
            <div class="reviews">
              <h3> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 12/07/2015</h3>
              <h4>
                <a href="https://www.facebook.com/pg/achievespeech/reviews/?ref=page_internal">Martinez Natalie  </a>
              </h4>
            </div>
        </div>
    </div>
